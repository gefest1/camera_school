import * as ffmpeg from 'fluent-ffmpeg';
// import WebSocket, * as WebSocket from 'ws';
import axios from 'axios';
import { Readable } from 'stream';
import * as fs from 'fs';
import { transporter } from './mail';

const bootstrap1 = async (image: string[], date: Date) => {
  // const date = new Date().toISOString();
  await transporter.sendMail({
    from: 'hellomik2002@gmail.com',
    to: 'hellomik2002@gmail.com',
    subject: 'Камеры',
    attachments: image.map((val, ind) => ({
      filename: date.toISOString() + ind + '.jpg',
      content: val,
      encoding: 'base64',
    })),

    text: 'Проверка',
  });
};
const bootss = (): Readable => {
  const readableStream = new Readable();
  readableStream._read = () => {};
  const { spawn } = require('child_process');

  const rtspUrl =
    'rtsp://admin:7072220226qwerty@192.168.1.10:554/Streaming/Channels/101/';
  const ffmpegArgs = [
    '-i',
    rtspUrl, // Input RTSP stream
    '-vf',
    'scale=1280:720,fps=1', // Resize to 640x360 and set frame rate to 1 fps
    '-f',
    'image2pipe', // Output format
    '-vcodec',
    'mjpeg', // Output codec
    '-',
  ];

  const ffmpeg = spawn('ffmpeg', ffmpegArgs);
  let buffer = Buffer.alloc(0);
  ffmpeg.stdout.on('data', (chunk) => {
    buffer = Buffer.concat([buffer, chunk]);

    // Try to find the end of a JPEG frame (FFD9)
    let frameEnd;
    while ((frameEnd = buffer.indexOf(Buffer.from([0xff, 0xd9]))) !== -1) {
      // Extract the complete frame
      const frame = buffer.subarray(0, frameEnd + 2);
      buffer = buffer.subarray(frameEnd + 2);

      // Convert the frame to base64
      const base64Frame = frame.toString('base64');

      // EMIT
      readableStream.push(base64Frame);
    }
  });

  // Handle errors
  ffmpeg.stderr.on('data', (data) => {
    // console.error('FFmpeg STDERR:', data.toString());
  });

  ffmpeg.on('close', (code) => {});
  return readableStream;
};

const sendData = async (arrayToSend: string[]) => {
  // console.log(new Date());
  const date = new Date();
  const responce = await axios.post(
    'https://6d14-2a03-32c0-1-6901-1f74-b56-a596-46d2.ngrok-free.app/process',
    {
      messages: [
        {
          role: 'user',
          content: 'Wrtie "ALARM" if you something is smoking',
          images: arrayToSend,
        },
      ],
    },
  );

  // console.log(responce.data['message']['content']);
  const wll: string = responce.data['message']['content'].toString();
  console.log(wll);
  if (wll.toLowerCase().includes('alarm')) {
    bootstrap1(arrayToSend, date);
  }
};

async function bootstrap() {
  let arrayToSend: string[] = [];
  let timeToSend: Date | null;

  const streamToRead = bootss();

  streamToRead.on('data', (chunk) => {
    timeToSend = timeToSend ?? new Date();
    arrayToSend.push(chunk.toString());

    if ((new Date().getTime() - timeToSend.getTime()) / 1000 > 5) {
      sendData([...arrayToSend]);
      arrayToSend = [];
      timeToSend = null;
    }
  });
  streamToRead.removeListener;
}

bootstrap();
